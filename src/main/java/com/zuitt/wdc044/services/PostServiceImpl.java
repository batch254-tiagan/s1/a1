package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // create a post function
    public void createPost(String stringToken, Post post) {
        // findByUsername to retrieve the user
        //Criteria for finding the user is from the jwtToken method getUsernameFromToken.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        // the title and content will come from the reqBody which is passed through the post identifier.
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        // author retrieve from the token
        newPost.setUser(author);

        // the actual saving of post in our table.
        postRepository.save(newPost);
    }


    // getting all posts
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    @Override
    public Iterable<Post> getPostsByUser(String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return postRepository.findByUser(user);
    }

    @Override
    public ResponseEntity<Object> updatePost(Long id, String stringToken, Post post) {
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post Updated", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("User not Authorized", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<Object> deletePost(Long id, String stringToken) {
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("User not Authorized", HttpStatus.UNAUTHORIZED);
        }
    }



}
